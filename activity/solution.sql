a. Find all artists that has letter d in its name.

Answer:SELECT * FROM artists WHERE name LIKE "%d%";

b. Find all songs that has a length of less than 230.

Answer:
SELECT * FROM songs WHERE length < 230;

c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and
song length.)

Answer:
SELECT albums.album_title,songs.song_name,songs.length FROM albums
	JOIN songs on albums.id = songs.album_id;

d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

Answer:
SELECT * FROM albums 
JOIN artists ON albums.artist_id = artists.id
WHERE albums.album_title LIKE "%a%";

e. Sort the albums in Z-A order. (Show only the first 4 records.)

Answer:
SELECT * FROM albums 
ORDER BY album_title DESC 
LIMIT 4;

f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

Answer:
SELECT * FROM albums
JOIN songs on albums.id = songs.album_id
ORDER BY album_title DESC;